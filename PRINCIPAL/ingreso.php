<?php
session_start();
$varsesion=$_SESSION['nombre_usuario'];
if($varsesion==null || $varsesion= ''){
echo 'ILEGAL... NO as ingresado datos para iniciar sesion!!';
die();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <meta charset="UTF-8">
		<title>Menu De Navegacion Con Listas</title>
		<link rel="stylesheet" type="text/css" href="ESTILOS/estilo2.css">
</head>
<body>
<nav>
  <img src="logouleam.png" alt="Logo de tu sitio web">
  <ul class="gestor">
  <li><a href="#" onclick="location.reload();">Facultades</a></li>
</ul>
<form id="search-form" action="#" method="get">
  <input type="search" placeholder="Buscar...">
  <button type="submit">Buscar</button>
</form>
  <ul> 
  <li>
    <a href="#" class="activos-fijos">Activos Fijos</a>
    <ul>
      <li><a href="#">Edificios y estructuras</a></li>
      <li><a href="#">Equipamiento y mobiliario</a></li>
      <li><a href="#">Vehículos</a></li>
      <li><a href="#">Tecnología</a></li>
    </ul>
  </li>
  <li>
    <a href="#" class="bienvenido"><?php echo $_SESSION['nombre_completo'] ?></a>
    <ul>
      <li><a href="#">Configuración</a></li>
      <li><a href="../DB/cerrarsesion.php">Salir</a></li>
    </ul>
  </li>
</nav>

<div class="contenedor-cuadros">
    <div class="cuadro">
    <h2><a href="#">FACULTAD CIENCIAS DE LA SALUD</a></h2>
      <ul>
        <li>Medicina</li>
        <li>Odontología</li>
        <li>Enfermería</li>
        <li>Fisioterapia</li>
        <li>Fonoaudiología</li>
        <li>Laboratorio Clínico</li>
        <li>Terapia Ocupacional</li>
        <li>Psicología</li>
      </ul>
    </div>
    <div class="cuadro">
      <h2><a href="#">FACULTAD CIENCIAS ADMINISTRATIVAS, CONTABLES Y COMERCIO</a></h2>
      <ul>
        <li>Administración de Empresas</li>
        <li>Mercadotecnia</li>
        <li>Contabilidad y Auditoria</li>
        <li>Auditoria y Control de Gestión</li>
        <li>Finanzas</li>
        <li>Comercio Exterior</li>
        <li>Gestión de Información Gerencial</li>
      </ul>
    </div>
    <div class="cuadro">
      <h2><a href="#">FACULTAD DE EDUCACIÓN TURISMO ARTES Y HUMANIDADES</a></h2>
      <ul>
        <li>Educación Inicial</li>
        <li>Educación Especial</li>
        <li>Psicología Educativa</li>
        <li>Educación Básica</li>
        <li>Pedagogía de la Actividad Física y el Deporte</li>
        <li>Pedagogía de la Lengua y la Literatura </li>
        <li>Pedagogía de los Idiomas Nacionales y Extranjeros</li>
        <li>Turismo</li>
        <li> Hospitalidad y Hotelería</li>
        <li>Artes Plásticas</li>
        <li>Sociología</li>
      </ul>
    </div>
    <div class="cuadro">
      <h2><a href="#">FACULTAD INGENIERÍA, INDUSTRIA Y CONSTRUCCIÓN</a></h2>
      <ul>
        <li>Ingenieria Civil</li>
        <li>Ingenieria Marítima</li>
        <li>Electricidad</li>
        <li>Arquitectura</li>
        <li>Ingeniería Industrial</li>
        <li>Ingeniería de Alimentos</li>
      </ul>
    </div>
    <div class="cuadro">
      <h2><a href="#">FACULTAD CIENCIAS DE LA VIDA Y TECNOLOGÍAS</a></h2>
      <ul>
        <li>Ingeneria Agropecuaria</li>
        <li>Agronegocios</li>
        <li>Ingenieria Agroindustrial</li>
        <li>Ingeniería Ambiental</li>
        <li>Ingeniería en Tecnologías de la información</li>
        <li>Ingenieria en Software</li>
        <li>Ingeniería en Sistema</li>
        <li>Biología</li>
      </ul>
    </div>
    <div class="cuadro">
      <h2></a><a href="#">FACULTAD CIENCIAS SOCIALES DERECHO Y BIENESTAR</a></h2>
      <ul>
        <li>Derecho</li>
        <li>Economía</li>
        <li>Trabajo Social</li>
        <li>Comunicación</li>
      </ul>
    </div>
  </div>
  <script src="script.js"></script>
</body>
</html>